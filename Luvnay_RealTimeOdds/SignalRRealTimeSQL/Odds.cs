﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalRRealTimeSQL
{
    public class Odds
    {
        public Int32 ID { get; set; }
        public string HomeTeam { get; set; }
        public decimal HomeWinOdds { get; set; }
        public decimal DrawOdds { get; set; }
        public decimal AwayWinOdds { get; set; }
        public string AwayTeam { get; set; }
    }
}
