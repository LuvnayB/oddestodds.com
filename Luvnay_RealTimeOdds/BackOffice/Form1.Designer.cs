﻿namespace BackOffice
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.iDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.homeTeamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.homeWinOddsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.drawOddsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awayWinOddsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.awayTeamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ID = new System.Windows.Forms.DataGridViewButtonColumn();
            this.oddsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.oDDESTODDSDataSet = new BackOffice.ODDESTODDSDataSet();
            this.oddsTableAdapter = new BackOffice.ODDESTODDSDataSetTableAdapters.OddsTableAdapter();
            this.View = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btn_save = new System.Windows.Forms.Button();
            this.txt_addAwayTeam = new System.Windows.Forms.TextBox();
            this.txt_addawayodd = new System.Windows.Forms.TextBox();
            this.txt_addDraw = new System.Windows.Forms.TextBox();
            this.txt_addhomeodd = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_addHomeTeam = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.txt_upd_AwayTeam = new System.Windows.Forms.TextBox();
            this.txt_updAwayOdd = new System.Windows.Forms.TextBox();
            this.txt_updDrawOdd = new System.Windows.Forms.TextBox();
            this.txt_updHomeOdd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_updHomeTeam = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oddsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.oDDESTODDSDataSet)).BeginInit();
            this.View.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDataGridViewTextBoxColumn,
            this.homeTeamDataGridViewTextBoxColumn,
            this.homeWinOddsDataGridViewTextBoxColumn,
            this.drawOddsDataGridViewTextBoxColumn,
            this.awayWinOddsDataGridViewTextBoxColumn,
            this.awayTeamDataGridViewTextBoxColumn,
            this.ID});
            this.dataGridView1.DataSource = this.oddsBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(14, 15);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1013, 442);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // iDDataGridViewTextBoxColumn
            // 
            this.iDDataGridViewTextBoxColumn.DataPropertyName = "ID";
            this.iDDataGridViewTextBoxColumn.HeaderText = "ID";
            this.iDDataGridViewTextBoxColumn.Name = "iDDataGridViewTextBoxColumn";
            this.iDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // homeTeamDataGridViewTextBoxColumn
            // 
            this.homeTeamDataGridViewTextBoxColumn.DataPropertyName = "HomeTeam";
            this.homeTeamDataGridViewTextBoxColumn.HeaderText = "HomeTeam";
            this.homeTeamDataGridViewTextBoxColumn.Name = "homeTeamDataGridViewTextBoxColumn";
            this.homeTeamDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // homeWinOddsDataGridViewTextBoxColumn
            // 
            this.homeWinOddsDataGridViewTextBoxColumn.DataPropertyName = "HomeWinOdds";
            this.homeWinOddsDataGridViewTextBoxColumn.HeaderText = "HomeWinOdds";
            this.homeWinOddsDataGridViewTextBoxColumn.Name = "homeWinOddsDataGridViewTextBoxColumn";
            this.homeWinOddsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // drawOddsDataGridViewTextBoxColumn
            // 
            this.drawOddsDataGridViewTextBoxColumn.DataPropertyName = "DrawOdds";
            this.drawOddsDataGridViewTextBoxColumn.HeaderText = "DrawOdds";
            this.drawOddsDataGridViewTextBoxColumn.Name = "drawOddsDataGridViewTextBoxColumn";
            this.drawOddsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // awayWinOddsDataGridViewTextBoxColumn
            // 
            this.awayWinOddsDataGridViewTextBoxColumn.DataPropertyName = "AwayWinOdds";
            this.awayWinOddsDataGridViewTextBoxColumn.HeaderText = "AwayWinOdds";
            this.awayWinOddsDataGridViewTextBoxColumn.Name = "awayWinOddsDataGridViewTextBoxColumn";
            this.awayWinOddsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // awayTeamDataGridViewTextBoxColumn
            // 
            this.awayTeamDataGridViewTextBoxColumn.DataPropertyName = "AwayTeam";
            this.awayTeamDataGridViewTextBoxColumn.HeaderText = "AwayTeam";
            this.awayTeamDataGridViewTextBoxColumn.Name = "awayTeamDataGridViewTextBoxColumn";
            this.awayTeamDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // ID
            // 
            this.ID.DataPropertyName = "ID";
            this.ID.HeaderText = "Delete?";
            this.ID.Name = "ID";
            this.ID.ReadOnly = true;
            this.ID.Text = "Delete";
            this.ID.UseColumnTextForButtonValue = true;
            // 
            // oddsBindingSource
            // 
            this.oddsBindingSource.DataMember = "Odds";
            this.oddsBindingSource.DataSource = this.oDDESTODDSDataSet;
            // 
            // oDDESTODDSDataSet
            // 
            this.oDDESTODDSDataSet.DataSetName = "ODDESTODDSDataSet";
            this.oDDESTODDSDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // oddsTableAdapter
            // 
            this.oddsTableAdapter.ClearBeforeFill = true;
            // 
            // View
            // 
            this.View.Controls.Add(this.tabPage1);
            this.View.Controls.Add(this.tabPage2);
            this.View.Controls.Add(this.tabPage3);
            this.View.Location = new System.Drawing.Point(12, 31);
            this.View.Name = "View";
            this.View.SelectedIndex = 0;
            this.View.Size = new System.Drawing.Size(1041, 576);
            this.View.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1033, 547);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "View / Delete";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_save);
            this.tabPage2.Controls.Add(this.txt_addAwayTeam);
            this.tabPage2.Controls.Add(this.txt_addawayodd);
            this.tabPage2.Controls.Add(this.txt_addDraw);
            this.tabPage2.Controls.Add(this.txt_addhomeodd);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.txt_addHomeTeam);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1033, 547);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Add";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(329, 173);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(289, 36);
            this.btn_save.TabIndex = 10;
            this.btn_save.Text = "Save Record";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.Btn_save_Click);
            // 
            // txt_addAwayTeam
            // 
            this.txt_addAwayTeam.Location = new System.Drawing.Point(779, 36);
            this.txt_addAwayTeam.Name = "txt_addAwayTeam";
            this.txt_addAwayTeam.Size = new System.Drawing.Size(178, 22);
            this.txt_addAwayTeam.TabIndex = 9;
            this.txt_addAwayTeam.TextChanged += new System.EventHandler(this.TextBox5_TextChanged);
            // 
            // txt_addawayodd
            // 
            this.txt_addawayodd.Location = new System.Drawing.Point(779, 87);
            this.txt_addawayodd.Name = "txt_addawayodd";
            this.txt_addawayodd.Size = new System.Drawing.Size(178, 22);
            this.txt_addawayodd.TabIndex = 8;
            // 
            // txt_addDraw
            // 
            this.txt_addDraw.Location = new System.Drawing.Point(451, 60);
            this.txt_addDraw.Name = "txt_addDraw";
            this.txt_addDraw.Size = new System.Drawing.Size(178, 22);
            this.txt_addDraw.TabIndex = 7;
            // 
            // txt_addhomeodd
            // 
            this.txt_addhomeodd.Location = new System.Drawing.Point(159, 91);
            this.txt_addhomeodd.Name = "txt_addhomeodd";
            this.txt_addhomeodd.Size = new System.Drawing.Size(178, 22);
            this.txt_addhomeodd.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(656, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Away Team Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(656, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Away Win Odd";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(360, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Draw Odd";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Home Win Odd";
            // 
            // txt_addHomeTeam
            // 
            this.txt_addHomeTeam.Location = new System.Drawing.Point(159, 33);
            this.txt_addHomeTeam.Name = "txt_addHomeTeam";
            this.txt_addHomeTeam.Size = new System.Drawing.Size(178, 22);
            this.txt_addHomeTeam.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Home Team Name";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.comboBox2);
            this.tabPage3.Controls.Add(this.button1);
            this.tabPage3.Controls.Add(this.btn_Update);
            this.tabPage3.Controls.Add(this.txt_upd_AwayTeam);
            this.tabPage3.Controls.Add(this.txt_updAwayOdd);
            this.tabPage3.Controls.Add(this.txt_updDrawOdd);
            this.tabPage3.Controls.Add(this.txt_updHomeOdd);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.txt_updHomeTeam);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Controls.Add(this.label6);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1033, 547);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Update";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(537, 50);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(159, 24);
            this.comboBox2.TabIndex = 23;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.ComboBox2_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(702, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 26);
            this.button1.TabIndex = 22;
            this.button1.Text = "search";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btn_Update
            // 
            this.btn_Update.Enabled = false;
            this.btn_Update.Location = new System.Drawing.Point(357, 239);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(267, 36);
            this.btn_Update.TabIndex = 21;
            this.btn_Update.Text = "Update Record";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // txt_upd_AwayTeam
            // 
            this.txt_upd_AwayTeam.Location = new System.Drawing.Point(810, 116);
            this.txt_upd_AwayTeam.Name = "txt_upd_AwayTeam";
            this.txt_upd_AwayTeam.Size = new System.Drawing.Size(178, 22);
            this.txt_upd_AwayTeam.TabIndex = 19;
            // 
            // txt_updAwayOdd
            // 
            this.txt_updAwayOdd.Location = new System.Drawing.Point(810, 167);
            this.txt_updAwayOdd.Name = "txt_updAwayOdd";
            this.txt_updAwayOdd.Size = new System.Drawing.Size(178, 22);
            this.txt_updAwayOdd.TabIndex = 18;
            // 
            // txt_updDrawOdd
            // 
            this.txt_updDrawOdd.Location = new System.Drawing.Point(482, 140);
            this.txt_updDrawOdd.Name = "txt_updDrawOdd";
            this.txt_updDrawOdd.Size = new System.Drawing.Size(178, 22);
            this.txt_updDrawOdd.TabIndex = 17;
            // 
            // txt_updHomeOdd
            // 
            this.txt_updHomeOdd.Location = new System.Drawing.Point(190, 171);
            this.txt_updHomeOdd.Name = "txt_updHomeOdd";
            this.txt_updHomeOdd.Size = new System.Drawing.Size(178, 22);
            this.txt_updHomeOdd.TabIndex = 16;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(687, 116);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Away Team Name";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(687, 167);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 14;
            this.label8.Text = "Away Win Odd";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(391, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Draw Odd";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(66, 171);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "Home Win Odd";
            // 
            // txt_updHomeTeam
            // 
            this.txt_updHomeTeam.Location = new System.Drawing.Point(190, 113);
            this.txt_updHomeTeam.Name = "txt_updHomeTeam";
            this.txt_updHomeTeam.Size = new System.Drawing.Size(178, 22);
            this.txt_updHomeTeam.TabIndex = 11;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(63, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Home Team Name";
            // 
            // comboBox1
            // 
            this.comboBox1.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.oddsBindingSource, "ID", true));
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "ID",
            "HomeTeam",
            "AwayTeam"});
            this.comboBox1.Location = new System.Drawing.Point(305, 50);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(164, 24);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(114, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(185, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Search record to update by:";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 641);
            this.Controls.Add(this.View);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oddsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.oDDESTODDSDataSet)).EndInit();
            this.View.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private ODDESTODDSDataSet oDDESTODDSDataSet;
        private System.Windows.Forms.BindingSource oddsBindingSource;
        private ODDESTODDSDataSetTableAdapters.OddsTableAdapter oddsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn homeTeamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn homeWinOddsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn drawOddsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn awayWinOddsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn awayTeamDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewButtonColumn ID;
        private System.Windows.Forms.TabControl View;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_addAwayTeam;
        private System.Windows.Forms.TextBox txt_addawayodd;
        private System.Windows.Forms.TextBox txt_addDraw;
        private System.Windows.Forms.TextBox txt_addhomeodd;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_addHomeTeam;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.TextBox txt_upd_AwayTeam;
        private System.Windows.Forms.TextBox txt_updAwayOdd;
        private System.Windows.Forms.TextBox txt_updDrawOdd;
        private System.Windows.Forms.TextBox txt_updHomeOdd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_updHomeTeam;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button1;
    }
}

