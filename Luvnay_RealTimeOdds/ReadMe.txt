1. Create a new database, and enable service broker.
(ALTER DATABASE [Database_name] SET ENABLE_BROKER;)

2. create a new table. script below:

-----------------------------
create table Odds
(
ID INT IDENTITY(1,1),
HomeTeam nvarchar(100),
HomeWinOdds decimal (9,2),
DrawOdds decimal (9,2),
AwayWinOdds decimal (9,2),
AwayTeam nvarchar(100)
)

-----------------------------

3. Update the App.Config files for both projects with the correct connectionString 
(i) BackOffice
(ii) SignalRRealTimeSQL

4. Start the solution.
It will display:
(i) one aspx page ==> for client side [real time]
(ii) one windows form ==> for Admin side (backend)

5. Live odds will appear on Client side, and can be updated on the windows form (Admin)

6. The windows form have three tabs:
(i) View / Delete ==> to view current Odds, and delete required row where necessary.
(ii) Add ==> to add a new Odd.
(iii) Update ==> to Update an existing odd.



