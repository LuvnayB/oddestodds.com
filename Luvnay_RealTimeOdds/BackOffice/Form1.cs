﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BackOffice
{
    public partial class Form1 : Form
    {
        public object DataGridView1 { get; private set; }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'oDDESTODDSDataSet.Odds' table. You can move, or remove it, as needed.
            this.oddsTableAdapter.Fill(this.oDDESTODDSDataSet.Odds);
            //tabPage3.SelectedIndexChanged += new EventHandler(tabControl1_SelectedIndexChanged);
            //FillComboBox2();

        }

        private void FillComboBox2()
        {
            String SearchValue = comboBox1.SelectedItem.ToString();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["BackOffice.Properties.Settings.ODDESTODDSConnectionString"].ToString()))
            {
                SqlCommand sqlCmd = new SqlCommand("select distinct " + SearchValue + " as [name] from Odds", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    comboBox2.Items.Add(sqlReader["name"].ToString());
                }

                sqlReader.Close();
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        var senderGrid = (DataGridView)sender;

        if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
            e.RowIndex >= 0)
        {
                string IDTODelete = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
                //dataGridView1.Rows[e.RowIndex].Cells[X].Value.ToString();

                //MessageBox.Show("hello world: "+ IDTODelete);
                deleteRow(IDTODelete);
                dataGridView1.Rows.RemoveAt(e.RowIndex);
            }
        }

        public static void deleteRow(string IDNumber)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BackOffice.Properties.Settings.ODDESTODDSConnectionString"].ToString()))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM Odds WHERE ID ='" + IDNumber + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void TextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox2.Items.Clear();
            FillComboBox2();
        }

        private void Label6_Click(object sender, EventArgs e)
        {

        }

        private void Btn_save_Click(object sender, EventArgs e)
        {
            String HomeTeamName = txt_addHomeTeam.Text;
            String AwayTeamName = txt_addAwayTeam.Text;
            String HomeOdd = txt_addhomeodd.Text;
            String AwayOdd = txt_addawayodd.Text;
            String DrawOdd = txt_addDraw.Text;


            if (HomeTeamName != "" && AwayTeamName != "" && HomeOdd != ""
                && AwayOdd != "" && DrawOdd != "")
            {
                SaveNewRecord(HomeTeamName, HomeOdd, DrawOdd, AwayOdd, AwayTeamName);
                MessageBox.Show("Record Saved");
                txt_addHomeTeam.Text = "";
                txt_addAwayTeam.Text = "";
                txt_addhomeodd.Text = "";
                txt_addawayodd.Text = "";
                txt_addDraw.Text = "";
                this.oddsTableAdapter.Fill(this.oDDESTODDSDataSet.Odds);
            }
            else
            {
                MessageBox.Show("All fields are mandatory, please re-try");
            }
        }

        private void SaveNewRecord(string homeTeamName, string homeOdd, string drawOdd, string awayOdd, string awayTeamName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BackOffice.Properties.Settings.ODDESTODDSConnectionString"].ToString()))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO Odds (HomeTeam, HomeWinOdds, DrawOdds, AwayWinOdds, AwayTeam) values ('" +
                     homeTeamName + "','" + homeOdd + "','" + drawOdd + "','" + awayOdd + "','" + awayTeamName + "')", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!(string.IsNullOrEmpty(comboBox1.Text)))
            {
                //Display top 1 row with selected value
                PopulateTextBox();

                btn_Update.Enabled = true;

            }
        }

        private void PopulateTextBox()
        {
            String SearchIndex = comboBox1.SelectedItem.ToString();
            String searchValue = comboBox2.SelectedItem.ToString();

            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["BackOffice.Properties.Settings.ODDESTODDSConnectionString"].ToString()))
            {
                SqlCommand sqlCmd = new SqlCommand("select top 1 HomeTeam, HomeWinOdds, DrawOdds, AwayWinOdds, AwayTeam from Odds where "
                    + SearchIndex + " = '" + searchValue + "'", sqlConnection);
                sqlConnection.Open();
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                while (sqlReader.Read())
                {
                    String HomeTeamName = sqlReader["HomeTeam"].ToString();
                    String AwayTeamName = sqlReader["AwayTeam"].ToString();
                    String HomeOdd = sqlReader["HomeWinOdds"].ToString();
                    String AwayOdd = sqlReader["AwayWinOdds"].ToString();
                    String DrawOdd = sqlReader["DrawOdds"].ToString();

                    txt_updHomeTeam.Text = HomeTeamName;
                    txt_upd_AwayTeam.Text = AwayTeamName;
                    txt_updHomeOdd.Text = HomeOdd;
                    txt_updAwayOdd.Text = AwayOdd;
                    txt_updDrawOdd.Text = DrawOdd;
                }

                sqlReader.Close();
            }
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            String HomeTeamName = txt_updHomeTeam.Text;
            String AwayTeamName = txt_upd_AwayTeam.Text;
            String HomeOdd = txt_updHomeOdd.Text;
            String AwayOdd = txt_updAwayOdd.Text;
            String DrawOdd = txt_updDrawOdd.Text;

            if (HomeTeamName != "" && AwayTeamName != "" && HomeOdd != ""
            && AwayOdd != "" && DrawOdd != "")
            {
                UpdateRecord(HomeTeamName, HomeOdd, DrawOdd, AwayOdd, AwayTeamName);
                MessageBox.Show("Record Saved");
                this.oddsTableAdapter.Fill(this.oDDESTODDSDataSet.Odds);
            }
            else
            {
                MessageBox.Show("All fields are mandatory, please re-try");
            }
        }

        private void UpdateRecord(string homeTeamName, string homeOdd, string drawOdd, string awayOdd, string awayTeamName)
        {
            try
            {
                String SearchIndex = comboBox1.SelectedItem.ToString();
                String searchValue = comboBox2.SelectedItem.ToString();
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["BackOffice.Properties.Settings.ODDESTODDSConnectionString"].ToString()))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("UPDATE Odds set HomeTeam = '" + homeTeamName + "', HomeWinOdds = '" + homeOdd + "'" +
                        ", DrawOdds = '" + drawOdd + "', AwayWinOdds = '" + awayOdd + "', AwayTeam = '" + awayTeamName + "' where "
                        + SearchIndex + " = '" + searchValue + "'"
                     , con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(string.Format("An error occurred: {0}", ex.Message));
            }
        }
    }
}
