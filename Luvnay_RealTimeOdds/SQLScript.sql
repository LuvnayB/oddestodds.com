
create table Odds
(
ID INT IDENTITY(1,1),
HomeTeam nvarchar(100),
HomeWinOdds decimal (9,2),
DrawOdds decimal (9,2),
AwayWinOdds decimal (9,2),
AwayTeam nvarchar(100)
)

insert into Odds (HomeTeam,HomeWinOdds,DrawOdds,AwayWinOdds,AwayTeam)
	values ("Barcelona","1.23","2.67","2.79","Ajax")
